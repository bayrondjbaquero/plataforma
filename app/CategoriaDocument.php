<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaDocument extends Model {
	public function documentos() {
		return $this->hasMany('App\Documento', 'categoria_id');
	}
}
