<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model {
	public function categoria() {
		return $this->belongsTo('App\CategoriaDocument');
	}
}
