<?php

namespace App\Http\Controllers\administrador;

use App\admin\Evento;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EventosController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$eventos = Evento::all();
		return view('eventos.index', compact('eventos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('eventos.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$validator = Validator::make($request->all(), [
				'title'       => 'required|string|max:255',
				'description' => 'required|string',
				'start_date'  => 'required|string',
				'end_date'    => 'required|string',
			]);

		// Si la validación falla
		if ($validator->fails()) {
			return back()->with('error', 'Ups, no se ha podido agregar el item')->withInput()->withErrors($validator->errors());
		}
		$store              = new Evento;
		$store->title       = $request->input('title');
		$store->description = $request->input('description');
		if ($request->hasFile('icon')) {
			$fileas            = $request->file('icon');
			$destinationPathas = 'images/eventos/';
			$fileas->move($destinationPathas, $fileas->getClientOriginalName());
			$store->icon = $destinationPathas.''.$fileas->getClientOriginalName();
		} else {
			$store->icon = 'images/perfil.png';
		}
		if ($request->hasFile('image')) {
			$fileaf            = $request->file('image');
			$destinationPathaf = 'images/eventos/';
			$fileaf->move($destinationPathaf, $fileaf->getClientOriginalName());
			$store->file = $destinationPathaf.''.$fileaf->getClientOriginalName();
		} else {
			$store->file = 'images/perfil.png';
		}
		$store->start_date = $request->input('start_date');
		$store->end_date   = $request->input('end_date');
		$store->save();
		return back()->with('success', 'Item creado correctamente');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$evento = Evento::find($id);
		return view('eventos.edit', compact('evento'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$store              = Evento::find($id);
		$store->title       = $request->input('title');
		$store->description = $request->input('description');
		if ($request->hasFile('icon')) {
			$fileas            = $request->file('icon');
			$destinationPathas = 'images/eventos/';
			$fileas->move($destinationPathas, $fileas->getClientOriginalName());
			$store->icon = $destinationPathas.''.$fileas->getClientOriginalName();
		}
		if ($request->hasFile('image')) {
			$fileaf            = $request->file('image');
			$destinationPathaf = 'images/eventos/';
			$fileaf->move($destinationPathaf, $fileaf->getClientOriginalName());
			$store->file = $destinationPathaf.''.$fileaf->getClientOriginalName();
		}
		$store->start_date = $request->input('start_date');
		$store->end_date   = $request->input('end_date');
		$store->update();
		return back()->with('success', 'Item actualizado correctamente');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$evento = Evento::find($id);
		$evento->delete();
		return back();
	}
}
