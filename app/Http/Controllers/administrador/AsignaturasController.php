<?php

namespace App\Http\Controllers\administrador;

use App\admin\Materia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AsignaturasController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$materias = Materia::all();
		return view('materias.index', compact('materias'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('materias.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$validator = Validator::make($request->all(), [
				'title' => 'required|string|max:255',
			]);

		// Si la validación falla
		if ($validator->fails()) {
			return back()->with('error', 'Ups, no se ha podido agregar su item')->withInput()->withErrors($validator->errors());
		}
		$store        = new Materia;
		$store->title = $request->input('title');
		$store->save();
		return back()->with('success', 'Item creado correctamente');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$asignatura = Materia::find($id);
		return view('materias.edit', compact('asignatura'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$validator = Validator::make($request->all(), [
				'title' => 'required|string|max:255',
			]);

		// Si la validación falla
		if ($validator->fails()) {
			return back()->with('error', 'Ups, no se ha podido agregar su item')->withInput()->withErrors($validator->errors());
		}
		$store        = Materia::find($id);
		$store->title = $request->input('title');
		$store->update();
		return back()->with('success', 'Item creado correctamente');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$store = Materia::find($id);
		$store->delete();
		return back();
	}
}
