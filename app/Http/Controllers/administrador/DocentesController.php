<?php

namespace App\Http\Controllers\administrador;

use App\Http\Controllers\Controller;
use App\Personal;
use App\Role;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

class DocentesController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$users = User::whereHas('roles', function ($q) {
				$q->where('name', 'docente');
			})->get();
		return view('docentes.index', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('docentes.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$validator = Validator::make($request->all(), [
				'name'     => 'required|string|max:255',
				'rol'      => 'required|string|max:255',
				'email'    => 'required|string|email|max:255|unique:users',
				'password' => 'required|string|min:6|confirmed',
			]);

		// Si la validación falla
		if ($validator->fails()) {
			return back()->with('error', 'Ups, no se ha podido agregar el docente')->withInput()->withErrors($validator->errors());
		}
		$store           = new User;
		$store->name     = $request->input('name');
		$store->email    = $request->input('email');
		$store->password = bcrypt($request->input('password'));
		$store->save();
		$store->roles()->attach(Role::where('name', $request->input('rol'))->first());
		$idUser           = $store->id;
		$persona          = new Personal;
		$persona->user_id = $idUser;
		$persona->save();
		return back()->with('success', 'Usuario creado correctamente');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$user = User::find($id);
		return view('docentes.edit', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$validator = Validator::make($request->all(), [
				'name'           => 'required|string|max:255',
				'identification' => 'required|numeric',
				'celular'        => 'required|numeric',
				'nacimiento'     => 'required|string|date|max:255',
				'correo'         => 'required|string|email|max:255',
				'address'        => 'required|string|max:255',
				'eps'            => 'required|string|max:255',
			]);

		// Si la validación falla
		if ($validator->fails()) {
			return back()->with('error', 'Ups, no se ha podido agregar el docente')->withInput()->withErrors($validator->errors());
		}
		$user       = User::find($id);
		$user->name = $request->input('name');
		$user->update();
		if ($user->update()) {
			$persona                 = Personal::find($request->input('idU'));
			$persona->identification = $request->input('identification');
			$persona->celular        = $request->input('celular');
			$persona->nacimiento     = $request->input('nacimiento');
			$persona->correo         = $request->input('correo');
			$persona->address        = $request->input('address');
			$persona->eps            = $request->input('eps');
			if ($request->hasFile('photo')) {
				$fileas            = $request->file('photo');
				$destinationPathas = 'images/docentes/';
				$fileas->move($destinationPathas, $fileas->getClientOriginalName());
				$persona->photo = $destinationPathas.''.$fileas->getClientOriginalName();
			} else {
				$persona->photo = 'images/perfil.png';
			}
			$persona->update();
			return back()->with('success', 'Usuario creado correctamente');
		}
		return back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$store = User::find($id);
		$store->delete();
		return back();
	}
}
