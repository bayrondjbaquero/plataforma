<?php

namespace App\Http\Controllers\administrador;

use App\admin\SubCurso;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubCursoController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$validator = Validator::make($request->all(), [
				'title'       => 'required|string|max:255',
				'description' => 'required|string',
			]);

		// Si la validación falla
		if ($validator->fails()) {
			return back()->with('error', 'Ups, no se ha podido agregar el item')->withInput()->withErrors($validator->errors());
		}
		$store              = new SubCurso;
		$store->title       = $request->input('title');
		$store->descripcion = $request->input('description');
		$store->curso_id    = $request->input('curso_id');
		$store->save();
		return back()->with('success', 'Item creado correctamente');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$subcurso = SubCurso::find($id);
		return view('cursos.edit', compact('subcurso'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$validator = Validator::make($request->all(), [
				'title'       => 'required|string|max:255',
				'description' => 'required|string',
			]);

		// Si la validación falla
		if ($validator->fails()) {
			return back()->with('error', 'Ups, no se ha podido agregar el item')->withInput()->withErrors($validator->errors());
		}
		$store              = SubCurso::find($id);
		$store->title       = $request->input('title');
		$store->descripcion = $request->input('description');
		$store->update();
		return back()->with('success', 'Item actualizó correctamente');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$store = SubCurso::find($id);
		$store->delete();
		return back()->with('success', 'Item eliminado correctamente');
	}
}
