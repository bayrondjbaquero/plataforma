<?php

namespace App\Http\Controllers\administrador;

use App\Documento;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DocumentosController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$validator = Validator::make($request->all(), [
				'title'       => 'required|string|max:255',
				'description' => 'required|string',
				'file'        => 'required|mimes:doc,pdf,docx,zip,xls',
			]);

		// Si la validación falla
		if ($validator->fails()) {
			return back()->with('error', 'Ups, no se ha podido agregar el item')->withInput()->withErrors($validator->errors());
		}
		$store              = new Documento;
		$nombreCategoria    = $request->input('nombre_categoria');
		$store->title       = $request->input('title');
		$store->description = $request->input('description');
		if ($request->hasFile('file')) {
			$fileas            = $request->file('file');
			$destinationPathas = 'documentos/'.$nombreCategoria.'/';
			$fileas->move($destinationPathas, $fileas->getClientOriginalName());
			$store->file = $destinationPathas.''.$fileas->getClientOriginalName();
		} else {
			$store->file = 'documentos/perfil.png';
		}
		$store->categoria_id = $request->input('categoria_id');
		$store->save();
		return back()->with('success', 'Item creado correctamente');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$documento = Documento::find($id);
		return view('documentos.edit', compact('documento'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$validator = Validator::make($request->all(), [
				'title'       => 'required|string|max:255',
				'description' => 'required|string',
			]);

		// Si la validación falla
		if ($validator->fails()) {
			return back()->with('error', 'Ups, no se ha podido agregar el item')->withInput()->withErrors($validator->errors());
		}
		$store              = Documento::find($id);
		$nombreCategoria    = $request->input('nombre_categoria');
		$store->title       = $request->input('title');
		$store->description = $request->input('description');
		if ($request->hasFile('file')) {
			$fileas            = $request->file('file');
			$destinationPathas = 'documentos/'.$nombreCategoria.'/';
			$fileas->move($destinationPathas, $fileas->getClientOriginalName());
			$store->file = $destinationPathas.''.$fileas->getClientOriginalName();
		}
		$store->save();
		return back()->with('success', 'Item creado correctamente');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$store = Documento::find($id);
		$store->delete();
		return back()->with('success', 'Item eliminado correctamente');
	}
}
