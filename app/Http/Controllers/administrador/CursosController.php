<?php

namespace App\Http\Controllers\administrador;

use App\admin\Curso;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CursosController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$users = User::whereHas('roles', function ($q) {
				$q->where('name', 'docente');
			})->get();
		$cursos = Curso::all();
		return view('cursos.categorias', compact('cursos', 'users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$validator = Validator::make($request->all(), [
				'title'       => 'required|string|max:255',
				'description' => 'required|string',
				'user_id'     => 'required',
			]);

		// Si la validación falla
		if ($validator->fails()) {
			return back()->with('error', 'Ups, no se ha podido agregar el item')->withInput()->withErrors($validator->errors());
		}
		$store              = new Curso;
		$store->title       = $request->input('title');
		$store->descripcion = $request->input('description');
		$store->user_id     = $request->input('user_id');
		$store->save();
		return back()->with('success', 'Item creado correctamente');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		$users = User::whereHas('roles', function ($q) {
				$q->where('name', 'docente');
			})->get();
		$curso = Curso::find($id);
		return view('cursos.ctedit', compact('curso', 'users'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$validator = Validator::make($request->all(), [
				'title'       => 'required|string|max:255',
				'description' => 'required|string',
			]);

		// Si la validación falla
		if ($validator->fails()) {
			return back()->with('error', 'Ups, no se ha podido agregar el item')->withInput()->withErrors($validator->errors());
		}
		$store              = Curso::find($id);
		$store->title       = $request->input('title');
		$store->descripcion = $request->input('description');
		$store->user_id     = $request->input('user_id');
		$store->update();
		return back()->with('success', 'Item actualizado correctamente');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$store = Curso::find($id);
		$store->delete();
		return back()->with('success', 'Item eliminado correctamente');
	}
}
