<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password', 'rol',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function sendPasswordResetNotification($token) {
		$this->notify(new ResetPasswordNotification($token));
	}
	public function roles() {
		return $this
			->belongsToMany('App\Role')
			->withTimestamps();
	}
	public function persona() {
		return $this->hasMany('App\Personal', 'user_id');
	}
	public function curso() {
		return $this->hasMany('App\Curso', 'user_id');
	}
	public function authorizeRoles($roles) {
		if ($this->hasAnyRole($roles)) {
			return true;
		}
		abort(401, 'Esta acción no está autorizada.');
	}

	public function hasAnyRole($roles) {
		if (is_array($roles)) {
			foreach ($roles as $role) {
				if ($this->hasRole($role)) {
					return true;
				}
			}
		} else {
			if ($this->hasRole($roles)) {
				return true;
			}
		}
		return false;
	}

	public function hasRole($role) {
		if ($this->roles()->where('name', $role)->first()) {
			return true;
		}
		return false;
	}
}
