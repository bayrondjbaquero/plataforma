<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model {
	public function titular() {
		return $this->belongsTo('App\User');
	}
	public function sub_cursos() {
		return $this->hasMany('App\admin\SubCurso', 'curso_id');
	}
}
