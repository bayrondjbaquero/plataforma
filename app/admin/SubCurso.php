<?php

namespace App\admin;

use Illuminate\Database\Eloquent\Model;

class SubCurso extends Model {
	public function curso() {
		return $this->belongsTo('App\Curso');
	}
}
