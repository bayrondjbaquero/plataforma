<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$role_admin       = Role::where('name', 'admin')->first();
		$role_coordinador = Role::where('name', 'coordinador')->first();
		$role_docente     = Role::where('name', 'docente')->first();
		$role_estudiante  = Role::where('name', 'estudiante')->first();

		$user           = new User();
		$user->name     = 'Admin';
		$user->email    = 'administrador@baykong.com';
		$user->password = bcrypt('secret');
		$user->save();
		$user->roles()->attach($role_admin);

		$user           = new User();
		$user->name     = 'Coordinador';
		$user->email    = 'coordinador@baykong.com';
		$user->password = bcrypt('secret');
		$user->save();
		$user->roles()->attach($role_coordinador);

		$user           = new User();
		$user->name     = 'Docente';
		$user->email    = 'docente@baykong.com';
		$user->password = bcrypt('secret');
		$user->save();
		$user->roles()->attach($role_docente);

		$user           = new User();
		$user->name     = 'Estudiante';
		$user->email    = 'estudiante@baykong.com';
		$user->password = bcrypt('secret');
		$user->save();
		$user->roles()->attach($role_estudiante);
	}
}
