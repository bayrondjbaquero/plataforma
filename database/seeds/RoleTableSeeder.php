<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$role              = new Role();
		$role->name        = 'admin';
		$role->description = 'Administrator del sistema';
		$role->save();

		$role              = new Role();
		$role->name        = 'coordinador';
		$role->description = 'Coordinadores';
		$role->save();

		$role              = new Role();
		$role->name        = 'docente';
		$role->description = 'Docentes';
		$role->save();

		$role              = new Role();
		$role->name        = 'estudiante';
		$role->description = 'Estudiantes';
		$role->save();
	}
}
