<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('personals', function (Blueprint $table) {
				$table->increments('id');
				$table->integer('identification');
				$table->integer('celular');
				$table->date('nacimiento');
				$table->string('correo');
				$table->string('address');
				$table->string('eps')->nullable();
				$table->string('photo')->nullable();
				$table->integer('user_id')->unsigned();
				$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('personals');
	}
}
