<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubCursosTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('sub_cursos', function (Blueprint $table) {
				$table->increments('id');
				$table->string('title');
				$table->string('descripcion');
				$table->integer('curso_id')->unsigned();
				$table->foreign('curso_id')->references('id')->on('cursos');
				$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('sub_cursos');
	}
}
