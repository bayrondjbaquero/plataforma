<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'HomeController@index')->name('inicio');

Auth::routes();

// Panel de Administración Routes...
Route::group(['namespace' => 'administrador', 'prefix' => 'admin', 'middleware' => ['auth', 'auth']], function () {
		Route::get('/home', 'DashboardController@index')->name('home');
		Route::resource('/coordinadores', 'CoordinadoresController');
		Route::resource('/docentes', 'DocentesController');
		Route::resource('/asignaturas', 'AsignaturasController');
		Route::resource('/categorias', 'CategoriaDocController');
		Route::resource('/documentos', 'DocumentosController');
		Route::resource('/eventos', 'EventosController');
		Route::resource('/cursos', 'CursosController');
		Route::resource('/subcurso', 'SubCursoController');
		// Admin Routes...
		/*Route::resource('/servicios', 'ServiciosController');*/
	});