<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/lato.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plataforma.css') }}" rel="stylesheet">
    <link href="{{ asset('css/web.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" class="col-md-12 cien">
        <div class="row cien">
            <div class="col-md-2 sidebar p-0 cien">
                <div class="p-2 border border-top-0 border-right-0 border-left-0">
                    <img src="{{ asset('images/LogoHG-blanco.png') }}" alt="" class="img-fluid d-block mx-auto">
                </div>
                <div class="p-3 border border-top-0 border-right-0 border-left-0">
                    <img src="{{ asset('images/perfil.png') }}" alt="" class="img-fluid rounded-circle d-block mx-auto mb-2">
                    <span class="text-bold text-white text-center mx-auto d-block">
                        {{ Auth::user()->name }}
                    </span>
                </div>
                <ul class="nav flex-column text-left text-regular">
                    <li class="nav-item {{ Request::is('admin/home') ? 'active' : '' }}">
                        <a class="nav-link text-white small " href="{{ route('home') }}">
                            <i class="fa fa-home mr-2"></i> Dashboard
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('admin/coordinadores') ? 'active' : '' }} {{ Request::is('admin/coordinadores/*') ? 'active' : '' }}" >
                        <a class="nav-link text-white small" href="{{ route('coordinadores.index') }}">
                            <i class="fa fa-users mr-2"></i> Coordinadores
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('admin/docentes') ? 'active' : '' }} {{ Request::is('admin/docentes/*') ? 'active' : '' }}">
                        <a class="nav-link text-white small" href="{{ route('docentes.index') }}">
                            <i class="fa fa-user-circle mr-2"></i> Docentes
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white small" href="#">
                            <i class="fa fa-address-card mr-2"></i> Estudiantes
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('admin/cursos') ? 'active' : '' }} {{ Request::is('admin/cursos/*') ? 'active' : '' }}">
                        <a class="nav-link text-white small" href="{{ route('cursos.index') }}">
                            <i class="fa fa-th mr-2"></i> Cursos
                        </a>
                    </li>
                    <li class="nav-item  {{ Request::is('admin/asignaturas') ? 'active' : '' }} {{ Request::is('admin/asignaturas/*') ? 'active' : '' }}">
                        <a class="nav-link text-white small" href="{{ route('asignaturas.index') }}">
                            <i class="fa fa-pencil mr-2"></i> Materias
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white small" href="#">
                            <i class="fa fa-sitemap mr-2"></i> Horario escolar
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white small" href="#">
                            <i class="fa fa-file-o mr-2"></i> Boletines Escolares
                        </a>
                    </li>
                    <li class="nav-item {{ Request::is('admin/eventos') ? 'active' : '' }} {{ Request::is('admin/eventos/*') ? 'active' : '' }}">
                        <a class="nav-link text-white small" href="{{ route('eventos.index') }}">
                            <i class="fa fa-calendar mr-2"></i> Eventos
                        </a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link text-white small" href="#">
                            <i class="fa fa-at mr-2"></i> Correos Padres de familia
                        </a>
                    </li> --}}
                    <li class="nav-item {{ Request::is('admin/categorias') ? 'active' : '' }} {{ Request::is('admin/categorias') ? 'active' : '' }} {{ Request::is('admin/documentos') ? 'active' : '' }} {{ Request::is('admin/documentos/*') ? 'active' : '' }}">
                        <a class="nav-link text-white small" href="{{ route('categorias.index') }}">
                            <i class="fa fa-book mr-2"></i> Documentos Legales
                        </a>
                    </li>
                    <li class="nav-item"><br>
                        <a class="nav-link text-white small" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out mr-2"></i> Cerrar sesión
                        </a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </ul>
            </div>
            <div class="col-md-10 p-0 bg-white">
                @include('menus.navbar')
                @yield('content')
            </div>
        </div>
    </div>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    <script>
        function myFunction() {
          var input, filter, table, tr, td, i;
          input = document.getElementById("myInput");
          filter = input.value.toUpperCase();
          table = document.getElementById("myTable");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
              if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            }
          }
        }
    </script>
    @yield('script')
</body>
</html>
