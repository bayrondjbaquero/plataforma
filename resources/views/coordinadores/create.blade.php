@extends('layouts.app')
@section('content')
    <div class="container-fluid text-regular">
		<nav class="navbar navbar-light  justify-content-between">
			<a class="navbar-brand text-bold"><i class="fa fa-plus-square"></i> Crear coordinador</a>
			<form class="form-inline">
				<a href="{{ route('coordinadores.index') }}" class="btn btn-danger btn-sm">
					<i class="fa fa-long-arrow-left"></i> Regresar
				</a>
			</form>
		</nav>
		<div class="x_panel">
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
		        @include('includes.alert')
		       	<!-- Formulario ADD Proyecto -->
	          	<div class="ml-4 col-md-6 col-xs-12">
	          		{!! Form::open(['route'=>'coordinadores.store','method'=> 'POST','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
		          		{!! csrf_field() !!}
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="">Nombre completo</label>
                            <input id="name" placeholder="juanito perez" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="">E-Mail Address</label>
                            <input id="email" placeholder="juanito.perez@gimnasiohowardgardner.com" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>


                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="">Contraseña</label>
                            <input id="password" placeholder="password12345" type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="">Confirmar Contraseña</label>
                            <input id="password-confirm" placeholder="password12345" type="password" class="form-control" name="password_confirmation" required>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <input id="role" type="hidden" class="form-control" name="rol" value="coordinador">
                            </div>
                        </div>
                    	<div class="form-group text-center">
                    		{!! Form::submit('+ Registrar usuario',['class'=>'d-block btn btn-primary btn-sm text-center']) !!}
                    	</div>
		          	{!! Form::close() !!}
	          	</div>
	      	</div>
		</div>
    </div>
@endsection
@section('script')
@endsection