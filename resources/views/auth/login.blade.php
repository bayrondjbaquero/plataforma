<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/lato.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/web.css') }}" rel="stylesheet">
</head>
<body class=" log">
<div class="container cien  ">
    <div class="row justify-content-center  pt-3">
        <div class="col-md-6 mt-5">
            <img src="{{ asset('images/LogoHG.png') }}" alt="" class="img-fluid d-block mx-auto logo animated fadeInDown">
            <div class="panel panel-default p-3  rounded">
                <div class="panel-heading text-bold mb-3 ">
                    <p class="text-bold text-center mx-auto">Bienvenido a la Plataforma Docente</p>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
                            <label for="email" class="col-md-4 control-label text-right">Usuario:</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control rounded-0" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} row">
                            <label for="password" class="col-md-4 control-label text-right">Contraseña:</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control rounded-0" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-4 ">
                                <button type="submit" class="btn btn-info btn-md d-block">
                                    <i class="fa fa-sign-in"></i>
                                    Ingresar
                                </button>

                                {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a> --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <p class="text-regular text-center text-danger mx-auto">
                    “The teaching that leaves its mark is not the one <br>
                    that is done head to head, but of heart to heart”
                </p>
            </div>
            <div class="row">
                <p class="text-bold text-center text-danger mx-auto">
                    Howard Gardner
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-sm-2 col-lg-3 ninos">
                <img src="{{ asset('images/ninos.png') }}" alt="" class="img-fluid">
            </div>
            <div class="col-md-2 col-sm-2 col-lg-3 senor">
                <img src="{{ asset('images/senor-gardner.png') }}" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</div>
{{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
</body>
</html>
