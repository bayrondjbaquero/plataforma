<!-- Modal -->
<div class="modal fade" id="sub" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar Sub curso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! Form::open(['route'=>'subcurso.store','method'=> 'POST','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
        {!! csrf_field() !!}
              <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                  <label for="title" class="">Nombre curso</label>
                  <input id="title" placeholder="..." type="text" class="form-control form-control-sm" name="title" value="{{ old('title') }}" required autofocus>
                  @if(isset($curso))
                  <input type="hidden" value="{{ $curso['id'] }}" name="curso_id">
                  @endif

                  @if ($errors->has('title'))
                      <span class="help-block">
                          <strong>{{ $errors->first('title') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                  <label for="description" class="">Descripción del subcurso</label>
                  <textarea name="description" class="form-control form-control-sm description" id="description">{{ old('description') }}</textarea>

                  @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
                  @endif
              </div>
            <div class="form-group text-center">
              {!! Form::submit('+ Registrar subcurso',['class'=>'d-block btn btn-primary btn-sm text-center']) !!}
            </div>
      {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="cursos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar Cursos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! Form::open(['route'=>'cursos.store','method'=> 'POST','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
        {!! csrf_field() !!}
              <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                  <label for="title" class="">Titulo curso</label>
                  <input id="title" placeholder="..." type="text" class="form-control form-control-sm" name="title" value="{{ old('title') }}" required autofocus>

                  @if ($errors->has('title'))
                      <span class="help-block">
                          <strong>{{ $errors->first('title') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                  <label for="description" class="">Descripción del curso</label>
                  <textarea name="description" class="form-control form-control-sm description" id="description">{{ old('description') }}</textarea>

                  @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                  <label for="user_id" class="">Docente titular</label>
                  <select name="user_id" class="form-control form-control-sm" id="">
                    <option value=""> seleccionar</option>
                    @foreach($users as $us)
                      <option value="{{ $us['id'] }}" >{{ $us['name'] }}</option>
                    @endforeach
                  </select>

                  @if ($errors->has('user_id'))
                      <span class="help-block">
                          <strong>{{ $errors->first('user_id') }}</strong>
                      </span>
                  @endif
              </div>
            <div class="form-group text-center">
              {!! Form::submit('+ Registrar cursos',['class'=>'d-block btn btn-primary btn-sm text-center']) !!}
            </div>
      {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>