@extends('layouts.app')
@section('content')
    <div class="container-fluid ">
		<nav class="navbar navbar-light  justify-content-between">
			<a class="navbar-brand text-bold-o">Cursos</a>
			<form class="form-inline">
				<a data-toggle="modal" data-target="#cursos" class="btn btn-success text-white btn-sm text-bold-o">
					<i class="fa fa-plus"></i> Nuevo curso
				</a>
				<input class="form-control ml-sm-2 form-control-sm" type="search" id="myInput" placeholder="Buscar..." aria-label="Search" onkeyup="myFunction()">
			</form>
		</nav>
    	<table class="table table-sm table-bordered dataTable text-regular-o" id="myTable">
    		<thead class="thead-dark small">
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Titulo</th>
                  <th scope="col">Descripción</th>
			      <th scope="col">Docente Titular</th>
			      {{-- <th scope="col">Correo personal</th>
			      <th scope="col">Dirección</th> --}}
			      {{-- <th scope="col">Eps</th> --}}
			      <th scope="col" colspan="2">Acciones</th>
			    </tr>
			</thead>
    		@foreach($cursos as $u)
    			<tr class="small text-center">
    				<th scope="row">{{$loop->iteration}}</th>
        			<td>
        				<span>{{ $u['title'] }}</span>
        			</td>
                    <td class="text-muted">
                        {!! \Illuminate\Support\Str::words($u['descripcion'],50) !!}
                    </td>
        			<td class="text-muted">
                        @foreach($users as $us)
                            @if($us['id'] == $u['user_id'])
                                {{ $us['name'] }}
                            @endif
                        @endforeach
        			</td>
        			<td>
        				<a href="{{ route('cursos.show', $u['id']) }}" class="btn btn-info btn-sm">
        					<i class="fa fa-pencil"></i> Editar
        				</a>
        			</td>
        			<td>
        				{!! Form::open(['route'=>['cursos.destroy',$u['id']],'method'=>'DELETE'])!!}
		               		<button onclick="return confirm('¿Está seguro de eliminar permanentemente a {{$u->title}}?');" class="btn btn-danger btn-sm text-left" type="submit"><i class="fa fa-remove" aria-hidden="true"></i> Eliminar</button>
		                {!! Form::close() !!}
        			</td>
        		</tr>
    		@endforeach
            @include('cursos.create')
    	</table>
    </div>
@endsection
@section('script')
@endsection