@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
    <div class="container-fluid text-regular ">
		<nav class="navbar navbar-light  justify-content-between">
			<a class="navbar-brand text-bold"><i class="fa fa-plus-square"></i> Editar: {{ $curso['title'] }}</a>
			<form class="form-inline">
				<a href="{{ route('cursos.index') }}" class="btn btn-danger btn-sm">
					<i class="fa fa-long-arrow-left"></i> Regresar
				</a>
			</form>
		</nav>
		<div class="x_panel bg-light">
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
		        @include('includes.alert')
		       	<!-- Formulario ADD Proyecto -->
	          	<div class="ml-4 col-md-12 col-xs-12">
	          		{!! Form::open(['route'=>['cursos.update', $curso['id']],'method'=> 'PUT','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
		          		{!! csrf_field() !!}
                        <div class="row">
                            <div class="col">
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                  <label for="title" class="">Titulo curso</label>
                                  <input id="title" placeholder="..." type="text" class="form-control form-control-sm" name="title" value="{{ $curso['title'] }}" required autofocus>

                                  @if ($errors->has('title'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('title') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                  <label for="description" class="">Descripción:</label>
                                  <textarea name="description" class="form-control form-control-sm description" id="description">{{ $curso['descripcion'] }}</textarea>

                                  @if ($errors->has('description'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('description') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                                  <label for="user_id" class="">Docente titular</label>
                                  <select name="user_id" class="form-control form-control-sm" id="">
                                    @foreach($users as $us)
                                      <option value="{{ $us['id'] }}" @if($us['id'] == $curso['user_id']) selected="selected" @endif>{{ $us['name'] }}</option>
                                    @endforeach
                                  </select>

                                  @if ($errors->has('user_id'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('user_id') }}</strong>
                                      </span>
                                  @endif
                              </div>
                            </div>
                            <div class="col">
                                <div class="form-group text-center">
                                {!! Form::submit('+ Actualizar curso',['class'=>'d-block btn btn-success btn-sm text-center']) !!}
                                </div>
                            </div>
                        </div>
		          	{!! Form::close() !!}
	          	</div>
	      	</div>
		</div>
    </div>
    <div class="container-fluid ">
        <nav class="navbar navbar-light  justify-content-between">
            <a class="navbar-brand text-bold-o">Sub categoria de cursos</a>
            <form class="form-inline">
                <a data-toggle="modal" data-target="#sub" class="btn btn-warning text-white btn-sm text-bold-o">
                    <i class="fa fa-plus"></i> Nuevo sub curso
                </a>
                <input class="form-control ml-sm-2 form-control-sm" type="search" id="myInput" placeholder="Buscar..." aria-label="Search" onkeyup="myFunction()">
            </form>
        </nav>
        <table class="table table-sm table-bordered dataTable text-regular-o" id="myTable">
            <thead class="thead-dark small">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Titulo</th>
                  <th scope="col">Descripción</th>
                  {{-- <th scope="col">Correo personal</th>
                  <th scope="col">Dirección</th> --}}
                  {{-- <th scope="col">Eps</th> --}}
                  <th scope="col" colspan="2">Acciones</th>
                </tr>
            </thead>
            @foreach($curso->sub_cursos as $u)
                <tr class="small text-center">
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>
                        <span>{{ $u['title'] }}</span>
                    </td>
                    <td class="text-muted">
                        {!! \Illuminate\Support\Str::words($u['descripcion'],50) !!}
                    </td>
                    <td>
                        <a href="{{ route('subcurso.show', $u['id']) }}?url={{ url()->previous() }}" class="btn btn-info btn-sm">
                            <i class="fa fa-pencil"></i> Editar
                        </a>
                    </td>
                    <td>
                        {!! Form::open(['route'=>['subcurso.destroy',$u['id']],'method'=>'DELETE'])!!}
                            <button onclick="return confirm('¿Está seguro de eliminar permanentemente a {{$u->title}}?');" class="btn btn-danger btn-sm text-left" type="submit"><i class="fa fa-remove" aria-hidden="true"></i> Eliminar</button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            @include('cursos.create')
        </table>
    </div>
@endsection
@section('script')
@endsection