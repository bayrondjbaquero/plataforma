@extends('layouts.app')
@section('content')
    <div class="container-fluid text-regular">
		<nav class="navbar navbar-light  justify-content-between">
			<a class="navbar-brand text-bold text-info"><i class="fa fa-plus-square"></i> Editar: {{ $asignatura['title'] }}</a>
			<form class="form-inline">
				<a href="{{ route('asignaturas.index') }}" class="btn btn-danger btn-sm">
					<i class="fa fa-long-arrow-left"></i> Regresar
				</a>
			</form>
		</nav>
		<div class="x_panel">
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
		        @include('includes.alert')
		       	<!-- Formulario ADD Proyecto -->
          		{!! Form::open(['route'=>['asignaturas.update', $asignatura->id],'method'=> 'PUT','files'=>"true", 'enctype'=>"multipart/form-data", 'class' => 'container-fluid']) !!}
		          		{!! csrf_field() !!}
						<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="">Nombre de asignatura</label>
                            <input id="title" placeholder="" type="text" class="form-control" name="title" value="{{ old('title') }}{{ $asignatura['title'] }}" required autofocus>

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    	<div class="form-group text-center">
                    		{!! Form::submit('+ Actualizar materia',['class'=>'d-block btn btn-success btn-sm text-center']) !!}
                    	</div>
	          	{!! Form::close() !!}
	      	</div>
		</div>
    </div>
@endsection
@section('script')
@endsection