@extends('layouts.app')
@section('content')
    <div class="container-fluid ">
		<nav class="navbar navbar-light  justify-content-between">
			<a class="navbar-brand text-bold-o">Asignaturas</a>
			<form class="form-inline">
				<a href="{{ route('asignaturas.create') }}" class="btn btn-success btn-sm text-bold-o">
					<i class="fa fa-plus"></i> Nueva asignatura
				</a>
				<input class="form-control ml-sm-2 form-control-sm" type="search" id="myInput" placeholder="Buscar..." aria-label="Search" onkeyup="myFunction()">
			</form>
		</nav>
    	<table class="table table-sm table-bordered dataTable text-regular-o" id="myTable">
    		<thead class="thead-dark small">
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Titulo</th>
			      {{-- <th scope="col">Correo personal</th>
			      <th scope="col">Dirección</th> --}}
			      {{-- <th scope="col">Eps</th> --}}
			      <th scope="col" colspan="2">Acciones</th>
			    </tr>
			</thead>
    		@foreach($materias as $u)
    			<tr class="small text-center">
    				<th scope="row">{{$loop->iteration}}</th>
        			<td>
        				<span>{{ $u['title'] }}</span>
        			</td>
        			<td>
        				<a href="{{ route('asignaturas.show', $u['id']) }}" class="btn btn-info btn-sm">
        					<i class="fa fa-pencil"></i> Editar
        				</a>
        			</td>
        			<td>
        				{!! Form::open(['route'=>['asignaturas.destroy',$u['id']],'method'=>'DELETE'])!!}
		               		<button onclick="return confirm('¿Está seguro de eliminar permanentemente a {{$u->name}}?');" class="btn btn-danger btn-sm text-left" type="submit"><i class="fa fa-remove" aria-hidden="true"></i> Eliminar</button>
		                {!! Form::close() !!}
        			</td>
        		</tr>
    		@endforeach
    	</table>
    </div>
@endsection
@section('script')
@endsection