@extends('layouts.app')
@section('content')
        <div class="container-fluid">
        	<h2 class="text-left text-dark text-bold">Mi Perfil</h2><br>
            @include('includes.alert')
        	{!! Form::open(['url'=>'contacto','method'=> 'POST']) !!}
            {!! csrf_field() !!}
                <div class="form-group row">
                    <div class="col-md-6">
                    	<label for="exampleFormControlInput1">Nombre completo</label>
                    	<input type="text" class="form-control rounded-0" placeholder="Nombres y apellidos:" value="{{ Auth::user()->name }}">
                    </div>
                    <div class="col-md-6">
                    	<label for="exampleFormControlInput1">Nombre de usuario</label>
                    	<input type="text" class="form-control rounded-0" placeholder="Nombres y apellidos:" value="{{ Auth::user()->email }}">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                    	<label for="exampleFormControlInput1">Identificación</label>
                    	<input type="number" class="form-control rounded-0" value="{{ Auth::user()->persona[0]->identification }}" name="identificacion" id="" placeholder="identificacion" required>
                    </div>
                    <div class="col-md-6">
                    	<label for="exampleFormControlInput1">Celular</label>
                    	<input type="text" class="form-control rounded-0" value="{{ Auth::user()->persona[0]->celular }}" name="celular" id="" placeholder="Celular" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                    	<label for="exampleFormControlInput1">Fecha nacimiento</label>
                    	<input type="date" class="form-control rounded-0" value="{{ Auth::user()->persona[0]->nacimiento }}" name="nacimiento" id="" placeholder="nacimiento" required>
                    </div>
                    <div class="col-md-6">
                    	<label for="exampleFormControlInput1">Correo electrónico</label>
                    	<input type="text" class="form-control rounded-0" name="correo" id="exampleFormControlInput2" placeholder="correo electronico" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                    	<label for="exampleFormControlInput1">Dirección de residencia</label>
                    	<input type="text" class="form-control rounded-0" value="{{ Auth::user()->persona[0]->address }}" name="address" id="" placeholder="address" required>
                    </div>
                    <div class="col-md-6">
                    	<label for="exampleFormControlInput1">EPS</label>
                    	<input type="text" class="form-control rounded-0" value="{{ Auth::user()->persona[0]->eps }}" name="eps" id="" placeholder="eps" required>
                    </div>
                </div>
                <div class="form-check text-center">
                    <div class="row">
                        <div class="col-md-7 col-xs-12">
                            <label class="form-check-label text-white small">
                                <input class="form-check-input" type="checkbox">
                                He leído y acepto los <br>términos y condiciones
                            </label>
                        </div>
                        <div class="col-md-5 col-xs-12">
                            <input class="btn btn-danger rounded-0 btn-md" value="Enviar" type="submit">
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}

        </div>
@endsection
