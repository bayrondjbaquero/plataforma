@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
    <div class="container-fluid text-regular ">
    <nav class="navbar navbar-light  justify-content-between">
      <a class="navbar-brand text-bold"><i class="fa fa-plus-square"></i> Editar: {{ $documento['title'] }}</a>
      <form class="form-inline">
        <a href="{{ $_GET['url'] }}" class="btn btn-danger btn-sm">
          <i class="fa fa-long-arrow-left"></i> Regresar
        </a>
      </form>
    </nav>
    <div class="x_panel bg-light">
          <div class="x_content row">
            @if(count($errors)!= 0)
                <div class="alert alert-warning text-white" role="alert">
            @foreach ($errors->all() as $error)
              <div>{{ $error }}</div>
            @endforeach
                </div>
            @endif
            @include('includes.alert')
            <!-- Formulario ADD Proyecto -->
              <div class="ml-4 col-md-8 col-xs-12">
                {!! Form::open(['route'=>['documentos.update', $documento['id']],'method'=> 'PUT','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
                  {!! csrf_field() !!}
                  <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title" class="">Titulo documento</label>
                    <input id="title" placeholder="..." type="text" class="form-control form-control-sm" name="title" value="{{ $documento['title'] }}" required autofocus>
                    @if(isset($_GET['nombre_categoria']))
                    <input type="hidden" value="{{ $_GET['nombre_categoria'] }}" name="nombre_categoria">
                    @endif

                    @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                    <label for="file" class="">Archivo</label>
                    <input id="file" type="file" class="form-control form-control-sm" name="file" value="{{ $documento['file'] }}{{ old('file') }}"  autofocus style="overflow: hidden;">

                    @if ($errors->has('file'))
                        <span class="help-block">
                            <strong>{{ $errors->first('file') }}</strong>
                        </span>
                    @endif
                </div>
                  <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="">Descripción:</label>
                    <textarea name="description" class="form-control form-control-sm description" id="description">{{ $documento['description'] }}</textarea>

                    @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group text-center">
                  {!! Form::submit('+ Actualizar documento',['class'=>'d-block btn btn-success btn-sm text-center']) !!}
                  </div>
                        </div>
                {!! Form::close() !!}
              </div>
          </div>
    </div>
@endsection
@section('script')
@endsection