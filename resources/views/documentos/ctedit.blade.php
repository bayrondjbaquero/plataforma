@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
    <div class="container-fluid text-regular ">
		<nav class="navbar navbar-light  justify-content-between">
			<a class="navbar-brand text-bold"><i class="fa fa-plus-square"></i> Editar: {{ $categoria['title'] }}</a>
			<form class="form-inline">
				<a href="{{ route('categorias.index') }}" class="btn btn-danger btn-sm">
					<i class="fa fa-long-arrow-left"></i> Regresar
				</a>
			</form>
		</nav>
		<div class="x_panel bg-light">
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
		        @include('includes.alert')
		       	<!-- Formulario ADD Proyecto -->
	          	<div class="ml-4 col-md-12 col-xs-12">
	          		{!! Form::open(['route'=>['categorias.update', $categoria['id']],'method'=> 'PUT','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
		          		{!! csrf_field() !!}
                        <div class="row">
                            <div class="col">
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                  <label for="title" class="">Titulo categoria</label>
                                  <input id="title" placeholder="..." type="text" class="form-control form-control-sm" name="title" value="{{ old('title') }}{{ $categoria['title'] }}" required autofocus>

                                  @if ($errors->has('title'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('title') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                  <label for="description" class="">Descripción:</label>
                                  <textarea name="description" class="form-control form-control-sm description" id="description">{{ old('description') }}{{ $categoria['description'] }}</textarea>

                                  @if ($errors->has('description'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('description') }}</strong>
                                      </span>
                                  @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group text-center">
                                {!! Form::submit('+ Actualizar categoria',['class'=>'d-block btn btn-success btn-sm text-center']) !!}
                                </div>
                            </div>
                        </div>
		          	{!! Form::close() !!}
	          	</div>
	      	</div>
		</div>
    </div>
    <div class="container-fluid ">
        <nav class="navbar navbar-light  justify-content-between">
            <a class="navbar-brand text-bold-o">Documentos</a>
            <form class="form-inline">
                <a data-toggle="modal" data-target="#documentos" class="btn btn-warning text-white btn-sm text-bold-o">
                    <i class="fa fa-plus"></i> Nuevo Documento
                </a>
                <input class="form-control ml-sm-2 form-control-sm" type="search" id="myInput" placeholder="Buscar..." aria-label="Search" onkeyup="myFunction()">
            </form>
        </nav>
        <table class="table table-sm table-bordered dataTable text-regular-o" id="myTable">
            <thead class="thead-dark small">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Titulo</th>
                  <th scope="col">File</th>
                  <th scope="col">Descripción</th>
                  {{-- <th scope="col">Correo personal</th>
                  <th scope="col">Dirección</th> --}}
                  {{-- <th scope="col">Eps</th> --}}
                  <th scope="col" colspan="2">Acciones</th>
                </tr>
            </thead>
            @foreach($categoria->documentos as $u)
                <tr class="small text-center">
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>
                        <span>{{ $u['title'] }}</span>
                    </td>
                    <td>
                        <a href="{{ asset($u['file']) }}" target="_blank">Ver documento</a>
                    </td>
                    <td class="text-muted">
                        {!! \Illuminate\Support\Str::words($u['description'],50) !!}
                    </td>
                    <td>
                        <a href="{{ route('documentos.show', $u['id']) }}?url={{ url()->previous() }}&nombre_categoria={{ $categoria['title'] }}" class="btn btn-info btn-sm">
                            <i class="fa fa-pencil"></i> Editar
                        </a>
                    </td>
                    <td>
                        {!! Form::open(['route'=>['documentos.destroy',$u['id']],'method'=>'DELETE'])!!}
                            <button onclick="return confirm('¿Está seguro de eliminar permanentemente a {{$u->title}}?');" class="btn btn-danger btn-sm text-left" type="submit"><i class="fa fa-remove" aria-hidden="true"></i> Eliminar</button>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            @include('documentos.create')
        </table>
    </div>
@endsection
@section('script')
@endsection