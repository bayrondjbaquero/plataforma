<!-- Modal -->
<div class="modal fade" id="documentos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar Documento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! Form::open(['route'=>'documentos.store','method'=> 'POST','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
        {!! csrf_field() !!}
              <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                  <label for="title" class="">Nombre documento</label>
                  <input id="title" placeholder="..." type="text" class="form-control form-control-sm" name="title" value="{{ old('title') }}" required autofocus>
                  @if(isset($categoria))
                  <input type="hidden" value="{{ $categoria['title'] }}" name="nombre_categoria">
                  <input type="hidden" value="{{ $categoria['id'] }}" name="categoria_id">
                  @endif

                  @if ($errors->has('title'))
                      <span class="help-block">
                          <strong>{{ $errors->first('title') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                  <label for="file" class="">Archivo</label>
                  <input id="file" type="file" class="form-control form-control-sm" name="file" value="{{ old('file') }}" required autofocus style="overflow: hidden;">

                  @if ($errors->has('file'))
                      <span class="help-block">
                          <strong>{{ $errors->first('file') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                  <label for="description" class="">Descripción del documento</label>
                  <textarea name="description" class="form-control form-control-sm description" id="description">{{ old('description') }}</textarea>

                  @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
                  @endif
              </div>
            <div class="form-group text-center">
              {!! Form::submit('+ Registrar documento',['class'=>'d-block btn btn-primary btn-sm text-center']) !!}
            </div>
      {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="categorias" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar Categoria</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {!! Form::open(['route'=>'categorias.store','method'=> 'POST','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
        {!! csrf_field() !!}
              <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                  <label for="title" class="">Titulo categoria</label>
                  <input id="title" placeholder="..." type="text" class="form-control form-control-sm" name="title" value="{{ old('title') }}" required autofocus>

                  @if ($errors->has('title'))
                      <span class="help-block">
                          <strong>{{ $errors->first('title') }}</strong>
                      </span>
                  @endif
              </div>
              <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                  <label for="description" class="">Descripción de la categoria</label>
                  <textarea name="description" class="form-control form-control-sm description" id="description">{{ old('description') }}</textarea>

                  @if ($errors->has('description'))
                      <span class="help-block">
                          <strong>{{ $errors->first('description') }}</strong>
                      </span>
                  @endif
              </div>
            <div class="form-group text-center">
              {!! Form::submit('+ Registrar categoria',['class'=>'d-block btn btn-primary btn-sm text-center']) !!}
            </div>
      {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>