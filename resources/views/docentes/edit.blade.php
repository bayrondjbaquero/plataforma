@extends('layouts.app')
@section('content')
    <div class="container-fluid text-regular">
		<nav class="navbar navbar-light  justify-content-between">
			<a class="navbar-brand text-bold text-info"><i class="fa fa-plus-square"></i> Editar usuario: {{ $user['name'] }}</a>
			<form class="form-inline">
				<a href="{{ route('docentes.index') }}" class="btn btn-danger btn-sm">
					<i class="fa fa-long-arrow-left"></i> Regresar
				</a>
			</form>
		</nav>
		<div class="x_panel">
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
		        @include('includes.alert')
		       	<!-- Formulario ADD Proyecto -->
          		{!! Form::open(['route'=>['docentes.update', $user->id],'method'=> 'PUT','files'=>"true", 'enctype'=>"multipart/form-data", 'class' => 'container-fluid']) !!}
		          	<div class="ml-4 mr-4 row bg-light pt-4">
		          		{!! csrf_field() !!}
						<div class="col-md-8 ">
							<div class="row">
								<div class="col">
									<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
			                            <label for="name" class="text-bold">Nombre completo</label>
			                            <input id="name" type="text" class="form-control form-control-sm" name="name" value="{{ $user['name'] }}" required autofocus>
			                            <input type="hidden" name="idU" value="{{ $user->persona[0]['id'] }}">

			                            @if ($errors->has('name'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('name') }}</strong>
			                                </span>
			                            @endif
			                        </div>
								</div>
								<div class="col">
									<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
			                            <label for="email" class="text-bold">E-Mail Address</label>
			                            <input id="email" placeholder="juanito.perez@gimnasiohowardgardner.com" type="email" class="form-control form-control-sm" name="email" value="{{ $user['email'] }}" required>

			                            @if ($errors->has('email'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('email') }}</strong>
			                                </span>
			                            @endif
			                        </div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group{{ $errors->has('identification') ? ' has-error' : '' }}">
			                            <label for="identification" class="text-bold">Identification</label>
			                            <input id="identification" placeholder="" type="text" class="form-control form-control-sm" name="identification" value="{{ $user->persona[0]['identification'] }}" required>
			                            @if ($errors->has('identification'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('identification') }}</strong>
			                                </span>
			                            @endif
			                        </div>
								</div>
								<div class="col">
									<div class="form-group{{ $errors->has('celular') ? ' has-error' : '' }}">
			                            <label for="celular" class="text-bold">Celular</label>
			                            <input id="celular" placeholder="" type="text" class="form-control form-control-sm" name="celular" value="{{ $user->persona[0]['celular'] }}" required>
			                            @if ($errors->has('celular'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('celular') }}</strong>
			                                </span>
			                            @endif
			                        </div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group{{ $errors->has('nacimiento') ? ' has-error' : '' }}">
			                            <label for="nacimiento" class="text-bold">Nacimiento</label>
			                            <input id="nacimiento" placeholder="" type="date" class="form-control form-control-sm" name="nacimiento" value="{{ $user->persona[0]['nacimiento'] }}" required>
			                            @if ($errors->has('nacimiento'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('nacimiento') }}</strong>
			                                </span>
			                            @endif
			                        </div>
								</div>
								<div class="col">
									<div class="form-group{{ $errors->has('correo') ? ' has-error' : '' }}">
			                            <label for="correo" class="text-bold">Correo personal</label>
			                            <input id="correo" placeholder="" type="email" class="form-control form-control-sm" name="correo" value="{{ $user->persona[0]['correo'] }}{{ old('correo') }}" required>
			                            @if ($errors->has('correo'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('correo') }}</strong>
			                                </span>
			                            @endif
			                        </div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
			                            <label for="address" class="text-bold">Dirección de residencia</label>
			                            <input id="address" placeholder="" type="text" class="form-control form-control-sm" name="address" value="{{ $user->persona[0]['address'] }}" required>
			                            @if ($errors->has('address'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('address') }}</strong>
			                                </span>
			                            @endif
			                        </div>
								</div>
								<div class="col">
									<div class="form-group{{ $errors->has('eps') ? ' has-error' : '' }}">
			                            <label for="eps" class="text-bold">EPS</label>
			                            <input id="eps" placeholder="" type="text" class="form-control form-control-sm" name="eps" value="{{ $user->persona[0]['eps'] }}" required>
			                            @if ($errors->has('eps'))
			                                <span class="help-block">
			                                    <strong>{{ $errors->first('eps') }}</strong>
			                                </span>
			                            @endif
			                        </div>
								</div>
							</div>
	                    	<div class="form-group text-center">
	                    		{!! Form::submit('+ Actualizar usuario',['class'=>'d-block mx-auto btn btn-warning text-white text-bold btn-sm text-center']) !!}
	                    	</div>
						</div>
						<div class="col-md-4">
							<div class="rounded border p-3" id="file-preview-zonsse" >
								<img id="file-preview" src="{{ asset($user->persona[0]['photo']) }}" alt="" class="img-fluid d-block mx-auto" >
							</div>
							<div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
	                            <label for="photo" class="text-bold">Foto</label>
	                            <input id="file-upload" placeholder="" type="file" class="form-control form-control-sm" name="photo" value="{{ $user['photo'] }}" required>
	                            @if ($errors->has('photo'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('photo') }}</strong>
	                                </span>
	                            @endif
	                        </div>
						</div>
		          	</div>
	          	{!! Form::close() !!}
	      	</div>
		</div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
        function readURL(input) {

		  if (input.files && input.files[0]) {
		    var reader = new FileReader();

		    reader.onload = function(e) {
		      $('#file-preview').attr('src', e.target.result);
		    }

		    reader.readAsDataURL(input.files[0]);
		  }
		}

        $("#file-upload").change(function() {
		  readURL(this);
		});
    </script>
@endsection