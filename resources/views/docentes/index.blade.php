@extends('layouts.app')
@section('content')
    <div class="container-fluid ">
		<nav class="navbar navbar-light  justify-content-between">
			<a class="navbar-brand text-bold-o">Docentes</a>
			<form class="form-inline">
				<a href="{{ route('docentes.create') }}" class="btn btn-success btn-sm text-bold-o">
					<i class="fa fa-plus"></i> Nuevo usuario
				</a>
				<input class="form-control ml-sm-2 form-control-sm" type="search" id="myInput" placeholder="Buscar..." aria-label="Search" onkeyup="myFunction()">
			</form>
		</nav>
    	<table class="table table-sm table-bordered dataTable text-regular-o" id="myTable">
    		<thead class="thead-dark small">
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Nombre y apellidos</th>
			      <th scope="col">Email</th>
			      <th scope="col">Foto</th>
			      <th scope="col">Identificación</th>
			      <th scope="col">Celular</th>
			      {{-- <th scope="col">Correo personal</th>
			      <th scope="col">Dirección</th> --}}
			      {{-- <th scope="col">Eps</th> --}}
			      <th scope="col" colspan="2">Acciones</th>
			    </tr>
			</thead>
    		@foreach($users as $u)
    			<tr class="small text-center">
    				<th scope="row">{{$loop->iteration}}</th>
        			<td>
        				<span>{{ $u['name'] }}</span>
        			</td>
        			<td>
        				<span>{{ $u['email'] }}</span>
        			</td>
        			@foreach($u->persona as $p)
        				<td>
	        				<img src="{{ asset($p['photo']) }}" alt="" class="h-10 img-fluid d-block" style="height: 30px;">
	        			</td>
	        			<td>
	        				<span>{{ $p['identification'] }}</span>
	        			</td>
	        			<td>
	        				<span>{{ $p['celular'] }}</span>
	        			</td>
	        			{{-- <td>
	        				<span>{{ $p['correo'] }}</span>
	        			</td>
	        			<td>
	        				<span>{{ $p['address'] }}</span>
	        			</td> --}}
	        			{{-- <td>
	        				<span>{{ $p['eps'] }}</span>
	        			</td> --}}
        			@endforeach
        			<td>
        				<a href="{{ route('docentes.show', $u['id']) }}" class="btn btn-info btn-sm">
        					<i class="fa fa-pencil"></i> Editar
        				</a>
        			</td>
        			<td>
        				{!! Form::open(['route'=>['docentes.destroy',$u['id']],'method'=>'DELETE'])!!}
		               		<button onclick="return confirm('¿Está seguro de eliminar permanentemente a {{$u->name}}?');" class="btn btn-danger btn-sm text-left" type="submit"><i class="fa fa-remove" aria-hidden="true"></i> Eliminar</button>
		                {!! Form::close() !!}
        			</td>
        		</tr>
    		@endforeach
    	</table>
    </div>
@endsection
@section('script')
@endsection