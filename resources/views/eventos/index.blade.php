@extends('layouts.app')
@section('content')
    <div class="container-fluid ">
		<nav class="navbar navbar-light  justify-content-between">
			<a class="navbar-brand text-bold-o">Eventos Educativos</a>
			<form class="form-inline">
				<a href="{{ route('eventos.create') }}" class="btn btn-success btn-sm text-bold-o">
					<i class="fa fa-plus"></i> Nuevo evento
				</a>
				<input class="form-control ml-sm-2 form-control-sm" type="search" id="myInput" placeholder="Buscar..." aria-label="Search" onkeyup="myFunction()">
			</form>
		</nav>
    	<table class="table table-sm table-bordered dataTable text-regular-o" id="myTable">
    		<thead class="thead-dark small">
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Titulo</th>
			      <th scope="col">Icono</th>
			      <th scope="col">Imagen</th>
			      <th scope="col">Descripción</th>
			      <th scope="col">Fecha inicio</th>
			      <th scope="col">Fecha final</th>
			      {{-- <th scope="col">Correo personal</th>
			      <th scope="col">Dirección</th> --}}
			      {{-- <th scope="col">Eps</th> --}}
			      <th scope="col" colspan="2">Acciones</th>
			    </tr>
			</thead>
    		@foreach($eventos as $u)
    			<tr class="small text-center">
    				<th scope="row">{{$loop->iteration}}</th>
        			<td>
        				<span>{{ $u['title'] }}</span>
        			</td>
        			<td>
        				<img src="{{ asset($u['icon']) }}" alt="" class=" img-fluid d-block" style="height: 20px;">
        			</td>
    				<td>
        				<img src="{{ asset($u['file']) }}" alt="" class=" img-fluid d-block" style="height: 30px;">
        			</td>
        			<td class="text-muted">
        				{!! \Illuminate\Support\Str::words($u['description'],50) !!}
        			</td>
        			<td>
        				<span>{{ $u['start_date'] }}</span>
        			</td>
        			<td>
        				<span>{{ $u['end_date'] }}</span>
        			</td>
        			<td>
        				<a href="{{ route('eventos.show', $u['id']) }}" class="btn btn-info btn-sm">
        					<i class="fa fa-pencil"></i> Editar
        				</a>
        			</td>
        			<td>
        				{!! Form::open(['route'=>['eventos.destroy',$u['id']],'method'=>'DELETE'])!!}
		               		<button onclick="return confirm('¿Está seguro de eliminar permanentemente a {{$u->title}}?');" class="btn btn-danger btn-sm text-left" type="submit"><i class="fa fa-remove" aria-hidden="true"></i> Eliminar</button>
		                {!! Form::close() !!}
        			</td>
        		</tr>
    		@endforeach
    	</table>
    </div>
@endsection
@section('script')
@endsection