@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
    <div class="container-fluid text-regular ">
		<nav class="navbar navbar-light  justify-content-between">
			<a class="navbar-brand text-bold"><i class="fa fa-plus-square"></i> Crear evento</a>
			<form class="form-inline">
				<a href="{{ route('eventos.index') }}" class="btn btn-danger btn-sm">
					<i class="fa fa-long-arrow-left"></i> Regresar
				</a>
			</form>
		</nav>
		<div class="x_panel bg-light">
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
		        @include('includes.alert')
		       	<!-- Formulario ADD Proyecto -->
	          	<div class="ml-4 col-md-8 col-xs-12">
	          		{!! Form::open(['route'=>'eventos.store','method'=> 'POST','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
		          		{!! csrf_field() !!}
                        <div class="row">
                            <div class="col">
                                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                    <label for="title" class="">Titulo del evento</label>
                                    <input id="title" placeholder="juanito perez" type="text" class="form-control form-control-sm" name="title" value="{{ old('title') }}" required autofocus>

                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group{{ $errors->has('icon') ? ' has-error' : '' }}">
                                    <label for="icon" class="">Icono thubmnail </label>
                                    <input id="icon" type="file" class="form-control form-control-sm" name="icon" value="{{ old('icon') }}" required autofocus style="overflow: hidden;">

                                    @if ($errors->has('icon'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('icon') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                    <label for="image" class="">Imagen (800x800)</label>
                                    <input id="image" type="file" class="form-control form-control-sm" name="image" value="{{ old('image') }}" required autofocus style="overflow: hidden;">

                                    @if ($errors->has('image'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                    @endif
                                </div>

                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="">Descripción</label>
                            <textarea name="description" class="form-control form-control-sm description" id="description">{{ old('description') }}</textarea>

                            @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }} ">
                                    <label for="start_date" class="">Fecha de inicio</label>
                                    <input id="datetimepicker1" type="text" class=" form-control form-control-sm" name="start_date" value="{{ old('start_date') }}" required >
                                    @if ($errors->has('start_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('start_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
        						<div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                                    <label for="end_date" class="">Fecha final</label>
                                    <input id="datetimepicker2" type="text" class="date form-control form-control-sm" name="end_date" value="{{ old('end_date') }}" required autofocus>

                                    @if ($errors->has('end_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('end_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    	<div class="form-group text-center">
                    		{!! Form::submit('+ Registrar evento',['class'=>'d-block btn btn-primary btn-sm text-center']) !!}
                    	</div>
		          	{!! Form::close() !!}
	          	</div>
	      	</div>
		</div>
    </div>
@endsection
@section('script')
    @include('includes.tinymce')
    <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/es-us.js') }}"></script>

    <script>
        $(function () {
            $('#datetimepicker1, #datetimepicker2').datetimepicker({
                format: 'YYYY-MM-DD hh:mm:ss',

                // daysOfWeekDisabled: [0, 6]
            });
        });
    </script>
@endsection